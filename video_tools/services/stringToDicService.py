import json
class StringToDic:
    def convertToDic(inputString):
        """TODO: Add the below code in a try catch block and log error"""
        intermediateString = inputString.replace("'", "\"")
        # Replacing (') with (")
        finalDic = json.loads(intermediateString)
        # using json.loads converting string to json
        return finalDic