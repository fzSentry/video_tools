from __future__ import absolute_import, unicode_literals
from celery import shared_task
import subprocess
from video_tools.services.redisStorageService import redisStore
import shutil
import uuid
import logging
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
logger = logging
layer = get_channel_layer()

@shared_task
def split(fileid, originalFile, toConvertFileList, performDic, folder, toSaveFolder, parentFolder):
    count = 0
    toConvDic = {}
    # the below code will parse the dictionay and add the data to a new dic (toConvDic)
    for var, var2 in performDic["videos"].items():
        newKey = "values" + str(count)
        var2["path"] = toConvertFileList[count]
        toConvDic[newKey] = var2
        count = count + 1
    print(toConvDic)
    for key, value in toConvDic.items():
        toConvFilePath = value["path"]
        start = value["start"]
        end = value["end"]
        print(toConvFilePath)
        print(start)
        print(end)
        subprocess.call(["ffmpeg", "-i", originalFile, "-ss", start, "-t", end, toConvFilePath])
    # create the zip file path to store the new splitted videos zip file
    newZipFile = str(toSaveFolder) + "/"+ str(uuid.uuid4())
    fullZipFile= newZipFile + ".zip"
    shutil.make_archive(newZipFile,'zip',folder)
    # delete the original splited videos folder
    shutil.rmtree(folder)
    # store the path in redis
    redisStore.addToRedis(file_id=fileid, file_path=fullZipFile)
    socketDataToSend = {
        "file_id": fileid,
        "status": "Complete"
    }
    # send to socket that the conversion is completed
    async_to_sync(layer.group_send)(
        'convert', {
            'type': 'send_toSocket',
            'message': socketDataToSend
        }
    )
    print("in send")
    # remove the original video file which was to be split
    try:
        shutil.rmtree(parentFolder)
    except Exception as e:
        logger.log(level=logging.DEBUG, msg=e)