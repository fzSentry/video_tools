import uuid
import os
from django.conf import settings
class ToConvert:
    def convertedFile(file, extension):
        # returns a new file path

        # concatenate a unique id with unique folder path
        fileUniqueId = "/Users/" + str(uuid.uuid4())
        # concatenate the unique folder path with absolute file path
        media_url = os.path.join(settings.BASE_DIR, "media")
       # concatenate with an extra slash
        convertedfilePath = media_url + fileUniqueId + "/"
        # create the unique folder
        os.mkdir(convertedfilePath)
        print(convertedfilePath)
        # create a unique file string of the toBe converted file
        convertFile = str(uuid.uuid4()) + extension
        # concatenate the above unique file string name with the folder path
        fullFilePath = convertedfilePath + convertFile
        return fullFilePath
    def convertedFolder(file):
        fileUniqueId = "/Users/" + str(uuid.uuid4())
        media_url = os.path.join(settings.BASE_DIR,"media")
        convertedfilePath = media_url + fileUniqueId + "/"
        os.mkdir(convertedfilePath)
        return convertedfilePath

    def convertedMultiFiles(count, extension):
        toConvertFileslist = []
        fileUniqueId1 = "/Users/" + str(uuid.uuid4())
        fileUniqueId2 = "/Users/" + str(uuid.uuid4())
        media_url = os.path.join(settings.BASE_DIR, "media")
        convertedfilePath1 = media_url + fileUniqueId1 + "/"
        convertedfilePath2 = media_url + fileUniqueId2 + "/"
        newFolderPath = convertedfilePath2
        os.mkdir(convertedfilePath1)
        os.mkdir(newFolderPath)
        print(convertedfilePath1)
        for i in range(count):
            convertFile = str(uuid.uuid4()) + extension
            fullFilePath = convertedfilePath1 + convertFile
            toConvertFileslist.append(fullFilePath)
        fileDic = {
            "fileList" : toConvertFileslist,
            "content_folder": convertedfilePath1,
            "new_directory": newFolderPath
        }
        return fileDic
