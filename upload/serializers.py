from rest_framework import serializers
from .models import Upload
# a serializer class for our models Upload
class FileSerializer(serializers.ModelSerializer):
  class Meta():
    model = Upload
    # the fields which needs to be serialized
    fields = ('file', 'fileID', 'procedure', 'timestamp')