from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import status
from upload.serializers import FileSerializer
from video_tools.VideoHandler import VideoHandler
import os
from mpi_video_tools.settings import BASE_DIR
from pathlib import Path
from video_tools.services.videoErrorCheckService import VidIntegrity
from video_tools.tasks.folder_delete_tasks import *


class FileView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        #POST DATA IS SERIALIZED HERE
        if file_serializer.is_valid():
            # Check if every data is valid with the required api parameters (models)
            file_serializer.save()
            # serialize and convert model data to a serialized json
            """Call Handle Upload CLass Here"""
            toConvertFileProcedure = file_serializer.data["procedure"]
            # Fetch data from serialized json
            fileData = file_serializer.data["file"]
            print("data" ,fileData)
            filePath = fileData.strip("/")
            # concatenate file path with absolute path
            fullFile = os.path.join(BASE_DIR, filePath)
            print("file path", fullFile)
            # fetch parent folder from full file path
            fileParentFolder = Path(str(fullFile)).parent
            # concatenate parent folder path with and extra slash (/)
            fileParentFolderDir = str(fileParentFolder) + "/"
            print("path", fileParentFolderDir)
            ###################################################
            # path the file to a video integrity check service to check if the given video is corrupted or not!
            integrityCheckResp = VidIntegrity.check(video=fullFile)
            if integrityCheckResp["msg"] == "damaged":
                convertResponse = {
                    "msg": "corrupted",
                    "id": file_serializer.data["fileID"],
                    "time": file_serializer.data["timestamp"],
                    "status": status.HTTP_200_OK,
                    "data": []
                }
                # if video is corrupted than delete the directory containing the corrupted video and send response to frontend
                deleteDir.delay(fileParentFolderDir)
                return Response(convertResponse)
            else:
                # If video is not corrupted pass video to video handler
                convertStatus = VideoHandler.handleVideoRequest(
                    toCheckfile=fullFile,
                    finalFileProcedure=toConvertFileProcedure,
                    file_id=file_serializer.data["fileID"],
                    parentFolder= fileParentFolderDir
                )
                #deleteDir.delay(fileParentFolderDir)
                print(convertStatus)
                if convertStatus["msg"] == "Success":

                    convertResponse = {
                        "msg": convertStatus["msg"],
                        "data": [],
                        "id": file_serializer.data["fileID"],
                        "time": file_serializer.data["timestamp"],
                        "status": status.HTTP_200_OK
                    }
                # return upload response
                    return Response((convertResponse), status=status.HTTP_200_OK)
                elif convertStatus["msg"] == "Bad-File-Type":
                    # if file is a bad file type
                    convertResponse = {
                        "msg": convertStatus["msg"],
                        "data": [],
                        "id": file_serializer.data["fileID"],
                        "time": file_serializer.data["timestamp"]
                    }
                # return upload response
                    return Response(data=convertResponse, status=status.HTTP_400_BAD_REQUEST)
                else:
                # if some errors occurs
                    convertResponse = {
                        "msg": convertStatus["msg"],
                        "data": [],
                        "id": file_serializer.data["fileID"],
                        "time": file_serializer.data["timestamp"]
                    }
                    return Response(data=convertResponse, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        else:
            # if file serializers has errors i.e all the valid parameters are not supplied!
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)