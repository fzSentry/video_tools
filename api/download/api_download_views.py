from rest_framework.views import APIView
from django.http import FileResponse
from downloads.serializers import DownloadSerializer
from rest_framework.parsers import MultiPartParser, FormParser
import os
import mimetypes
from wsgiref.util import FileWrapper
from downloads.DownloadGlobals import *
from video_tools.services.redisStorageService import redisRetrive
from video_tools.tasks.after_request_tasks import *
import pathlib
class FileDownloadApiView(APIView):

    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        download_serializer = DownloadSerializer(data=request.data)
        if download_serializer.is_valid():
            # check serializer data valid (api params valid)
            download_serializer.save()
        fileid = download_serializer.data["fileID"]
        the_file = redisRetrive.getFromRedis(file_id=fileid)
        filename = os.path.basename(the_file)
        # send a download file response by reading a file in some specific bytes chunks
        # Chunk size to be load tested
        response = FileResponse(
            FileWrapper(
                open(
                    the_file,
                    'rb'
                ),
                DOWNLOAD_CHUNK_SIZE
            ),
            content_type=mimetypes.guess_type(the_file)[0]
        )
        response['Content-Length'] = os.path.getsize(the_file)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        path = pathlib.PurePath(the_file)
        folderName = path.parent
        print(folderName)
        fileDic = {
            "file" : fileid,
            "parentFolder" : str(folderName)
        }
        # sending the parent folder of the file to after request task which will delete the folder after the
        # file is downloaded
        afterResponseTask = my_task.delay(fileDic)
        #IMP NOT A CODE ERROR DO NOT EDIT THE ABOVE LINE
        return response