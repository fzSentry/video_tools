from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import status
from merge_videos.merge_upload.serializers import FileSerializer
import os
from mpi_video_tools.settings import BASE_DIR
from video_tools.services.redisStorageService import redisStore
from video_tools.services.videoErrorCheckService import VidIntegrity
# used to upload videos which are to be merged
class FileView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            """Call Handle Upload CLass Here"""
            fileID = file_serializer.data["fileID"]
            fileData = file_serializer.data["file"]
            filePath = fileData.strip("/")
            fullFile = os.path.join(BASE_DIR, filePath)
            print("file path", fullFile)
            integrityCheckResp = VidIntegrity.check(video=fullFile)
            if integrityCheckResp["msg"] == "damaged":
                convertResponse = {
                    "msg": "corrupted",
                    "id": file_serializer.data["fileID"],
                    "time": file_serializer.data["timestamp"],
                    "status": status.HTTP_200_OK,
                    "data": []
                }
                return Response(convertResponse)
            else:
                redisStore.addToRedis(file_id=fileID, file_path=fullFile)
                convertResponse = {
                    "msg": "success",
                    "id": file_serializer.data["fileID"],
                    "time": file_serializer.data["timestamp"],
                    "status": status.HTTP_200_OK,
                    "data": []
                }
                return Response(convertResponse)

        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)