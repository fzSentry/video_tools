from django.urls import path
from . import api_download_views as views

urlpatterns = [
    path('make_download/', views.FileDownloadApiView.as_view(), name="make_downloads")
]