from celery import Celery
from post_request_task.task import PostRequestTask
app = Celery('mpi_video_tools', task_cls=PostRequestTask)
import shutil
# this scripts uses mozilla's after response library to perform tasks after a response is being sent to user
@app.task
def my_task(directory):
    folder = directory["parentFolder"]
    try:
        shutil.rmtree(folder)
    except Exception as e:
        print(e)