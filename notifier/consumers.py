# chat/consumers.py
from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync
import json
class StatusConsumer(WebsocketConsumer):
# main consumers file
    def connect(self):
    # establish a socket connections
        async_to_sync(self.channel_layer.group_add)(
            "convert",
            self.channel_name
        )
        self.accept()
        print("new channel created")
    def disconnect(self, close_code):
    # code to handle socket disconnect event
        print("disconnected")
        async_to_sync(self.channel_layer.group_discard)(
            'convert',
            self.channel_name
        )

    def send_toSocket(self, event):
    # function to send data to socket
        message = event["message"]
        print("in socket func")
        self.send(
            text_data=json.dumps({
            'message': message
        }))