from __future__ import absolute_import, unicode_literals
from celery import shared_task
import subprocess
import logging
from video_tools.services.redisStorageService import redisStore
import shutil
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

logger = logging
layer = get_channel_layer()
@shared_task
def convert(fileid, originalFile, toConvertFile, parentFolder, scale, start, end):
    # celery function to create a video to gif
    if scale == "300":
        scaleQuery = "scale"+"="+scale+":-1"
        """TODO: log the output of the subprocess.run"""
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)
        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
                'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")


        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "320":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale"+"="+scale+":-1"
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)


        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "400":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale"+"="+scale+":-1"
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "480":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale"+"="+scale+":-1"
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "500":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale"+"="+scale+":-1"
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "540":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale"+"="+scale+":-1"
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "600":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale"+"="+scale+":-1"
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "800":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale"+"="+scale+":-1"
        subprocess.run(["ffmpeg",
                        "-i",
                        originalFile,
                        "-r",
                        "15",
                        "-vf",
                        scaleQuery,
                        "-ss", start,
                        "-to", end,
                        toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "1200,300":
        """TODO: log the output of the subprocess.run"""
        scaleArray = scale.split(',')
        scaleQuery = "scale" + "=" + scaleArray[0] + ":" + scaleArray[1]
        subprocess.call(["ffmpeg",
                         "-i",
                         originalFile,
                         "-r",
                         "15",
                         "-vf",
                         scaleQuery,
                         "-ss", start,
                         "-to", end,
                         toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    elif scale == "300,1200":
        """TODO: log the output of the subprocess.run"""
        scaleQuery = "scale" + "=" + scale
        scaleArray = scale.split(',')
        scaleQuery = "scale" + "=" + scaleArray[0] + ":" + scaleArray[1]
        subprocess.call(["ffmpeg",
                         "-i",
                         originalFile,
                         "-r",
                         "15",
                         "-vf",
                         scaleQuery,
                         "-ss", start,
                         "-to", end,
                         toConvertFile])
        redisStore.addToRedis(file_id=fileid, file_path=toConvertFile)

        socketDataToSend = {
            "file_id": fileid,
            "status": "Complete"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
    else:
        socketDataToSend = {
            "file_id": fileid,
            "status": "Bad-Request"
        }

        async_to_sync(layer.group_send)(
            'convert', {
                'type': 'send_toSocket',
                'message': socketDataToSend
            }
        )
        print("in send")
        try:
            shutil.rmtree(parentFolder)
        except Exception as e:
            logger.log(level=logging.DEBUG, msg=e)
