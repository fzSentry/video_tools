# The first instruction is what image we want to base our container on
# We Use an official ubuntu 18.04
FROM ubuntu:18.04
FROM python:3.7
RUN apt-get update \
  && apt-get install -y python3.7-dev \
  && pip3 install --upgrade pip
# Install My sql client
RUN apt-get -y install default-libmysqlclient-dev

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1

## create root directory for our project in the container
RUN mkdir /mpi_video_tools
#
## Set the working directory to /mpi_video_tools
WORKDIR /mpi_video_tools
#
## Copy the current directory contents into the container at /mpi_video_tools
ADD . /mpi_video_tools/
#
## Install any needed packages specified in requirements.txt
RUN pip3 install -r requirements.txt

RUN apt-get -y install ffmpeg
