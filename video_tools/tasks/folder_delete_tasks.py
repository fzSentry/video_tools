from __future__ import absolute_import, unicode_literals
import shutil
import logging
from celery import shared_task
logger = logging
# a folder delete celery tasks use to delete folders if they are corrupted or bad requests
@shared_task
def deleteDir(directory):
    try:
        shutil.rmtree(directory)
    except Exception as e:
            logger.log(msg=e)