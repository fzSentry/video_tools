from .VideoToolsPrerequisites import VideoToolsPrerequisites
from .services.convertedFilePathCreationService import ToConvert
from .services.stringToDicService import StringToDic
from .tasks.video_to_gif_tasks import *
from .tasks.video_to_images_tasks import *
from .tasks.video_trim_tasks import *
from .tasks.video_split_tasks import *
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync



class VideoHandler():
    def handleVideoRequest(file_id , toCheckfile, finalFileProcedure, parentFolder):
        # in precheck lets check that the video is of valid video mime type
        precheck = VideoToolsPrerequisites.checkVideoPreq(file=toCheckfile)
        if precheck == True:
            # if precheck is true convert the string received during response to dictionary
            procedureDic = StringToDic.convertToDic(inputString=finalFileProcedure)
            # fetch procedure from dic
            toPerformProcedure = procedureDic["procedure"]

            ##################################################
            if toPerformProcedure == ".gif":
                # if the procedure is gif fetch start time, end time and scale from dic
                startTime = procedureDic["start"]
                endTime = procedureDic["end"]
                scaleFactor = procedureDic["scale"]
                """Convert video to .gif"""
                print("in gif")
                # creating the dynamic file path of the to be created new file along with actually creating the folder where
                # is stored
                convertedFilePath = ToConvert.convertedFile(file=toCheckfile, extension=toPerformProcedure)
                # send a socket data to the front end about the status of the file to be "on-going"
                socketDataToSend = {
                    "file_id": file_id,
                    "status": "On-Going"
                }
                layer = get_channel_layer()
                async_to_sync(layer.group_send)(
                    'convert',{
                        'type': 'send_toSocket',
                        'message': socketDataToSend
                    }
                )
                print("sent to socket ongoing")
                print("toCheckFile", toCheckfile)
                print("to convert", convertedFilePath)
                print("parent folder", parentFolder)
                # call the celery task below and pass all the data to the function
                videoConversionTask = convert.delay(
                    originalFile=toCheckfile,
                    toConvertFile=convertedFilePath,
                    fileid=file_id,
                    parentFolder=parentFolder,
                    scale=scaleFactor,
                    start=startTime,
                    end=endTime
                )
                """TODO: Implement Socket Code Here"""
                convertStatus = {
                    "msg": "Success",
                    "data": []
                }
                return convertStatus
            elif toPerformProcedure == "images":
                # video to images function
                """Convert video to images"""
                convertedFilePath = ToConvert.convertedFolder(file=toCheckfile)
                socketDataToSend = {
                    "file_id": file_id,
                    "status": "On-Going"
                }
                layer = get_channel_layer()
                async_to_sync(layer.group_send)(
                    'convert', {
                        'type': 'send_toSocket',
                        'message': socketDataToSend
                    }
                )
                print("sent to socket ongoing")
                videoConversionTask = convertImages.delay(
                    originalFile=toCheckfile,
                    toConvertFilePath=convertedFilePath,
                    fileid=file_id,
                    parentFolder=parentFolder
                )
                convertStatus = {
                    "msg": "Success",
                    "data": []
                }
                return convertStatus
            elif toPerformProcedure =="trim":
                fileExtension = ".mp4"
                startTime = procedureDic["start"]
                endTime = procedureDic["end"]
                convertedFilePath = ToConvert.convertedFile(file=toCheckfile, extension=fileExtension)
                socketDataToSend = {
                    "file_id": file_id,
                    "status": "On-Going"
                }
                layer = get_channel_layer()
                async_to_sync(layer.group_send)(
                    'convert', {
                        'type': 'send_toSocket',
                        'message': socketDataToSend
                    }
                )
                print("sent to socket ongoing")
                videoTrimTask = trim.delay(
                    originalFile=toCheckfile,
                    toConvertFile=convertedFilePath,
                    fileid=file_id,
                    start=startTime,
                    end=endTime,
                    parentFolder=parentFolder
                )


                convertStatus = {
                    "msg": "Success",
                    "data": []
                }
                return convertStatus

            elif toPerformProcedure == "split":
                vidCount = procedureDic["count"]
                """
                Pass dic to task
                """
                convertedFilesDic = ToConvert.convertedMultiFiles(
                    count=vidCount,
                    extension=".mp4"
                )
                content_folder = convertedFilesDic["content_folder"]
                fileList = convertedFilesDic["fileList"]
                newFolder = convertedFilesDic["new_directory"]
                socketDataToSend = {
                    "file_id": file_id,
                    "status": "On-Going"
                }
                layer = get_channel_layer()
                async_to_sync(layer.group_send)(
                    'convert', {
                        'type': 'send_toSocket',
                        'message': socketDataToSend
                    }
                )
                print("sent to socket ongoing")
                """
                pass the above list & dic to tasks
                """
                videoSpltTask = split.delay(
                    originalFile=toCheckfile,
                    toConvertFileList=fileList,
                    fileid=file_id,
                    toSaveFolder = newFolder,
                    folder = content_folder,
                    performDic = procedureDic,
                    parentFolder=parentFolder
                )

                convertStatus = {
                    "msg": "Success",
                    "data": []
                }
                return convertStatus


        else:
            convertStatus = {
                "msg": "Bad-File-Type",
                "data": []
            }
            return convertStatus
