from django.conf.urls import url
from .api_merge_video_uploads_views import FileView
from .api_merge_video_convert_views import MergeView
urlpatterns = [
  url('upload/', FileView.as_view(), name='file-upload'),
  url('videos/', MergeView.as_view(), name='file-upload'),
]
