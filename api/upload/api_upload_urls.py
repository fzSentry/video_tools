from django.conf.urls import url
from .api_upload_views import FileView
urlpatterns = [
  url('upload/', FileView.as_view(), name='file-upload'),
]
