from rest_framework import serializers
from downloads.models import Download
# serializer for dowload model
class DownloadSerializer(serializers.ModelSerializer):
  class Meta():
    model = Download
    fields = ["fileID"]