"""**********************************USE DEPRECATED *******************************************"""
# Pub Sub was used in earlier versions of this project now using web sockets ******************

# import redis
# from mpi_video_tools.settings import \
# redis_host, \
# redis_db, \
# redis_password,\
# redis_port
# class PubSub:
#     def publish(fileId, status):
#         redisClient = redis.StrictRedis(
#             host=redis_host,
#             port=redis_port,
#             db=redis_db,
#             password=redis_password,
#             ssl=True
#         )
#         redisClient.publish(fileId, status)