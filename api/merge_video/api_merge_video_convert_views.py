from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from rest_framework import status
from merge_videos.merge_convert.serializers import FileSerializer
from video_tools.tasks.merge_video_tasks import *
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync

layer = get_channel_layer()
# used to merge uploaded videos
class MergeView(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def post(self, request, *args, **kwargs):
        file_serializer = FileSerializer(data=request.data)
        if file_serializer.is_valid():
            file_serializer.save()
            """Call Handle Upload CLass Here"""
            fileID = file_serializer.data["fileID"]
            convertID = file_serializer.data["convertID"]
            socketDataToSend = {
                "file_id": fileID,
                "status": "On-Going"
            }
            layer = get_channel_layer()
            async_to_sync(layer.group_send)(
                'convert', {
                    'type': 'send_toSocket',
                    'message': socketDataToSend
                }
            )
            print("sent to socket ongoing")
            mergeVideo = merge.delay(
                file_ids=fileID,
                convert_id = convertID,
            )
            convertResponse = {
                "msg": "success",
                "status": status.HTTP_200_OK,
                "data" : []
            }
            return Response(convertResponse)

        else:
            return Response(file_serializer.errors, status=status.HTTP_400_BAD_REQUEST)