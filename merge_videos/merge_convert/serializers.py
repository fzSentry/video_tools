from rest_framework import serializers
from .models import Merge
# serializer for Merge model
class FileSerializer(serializers.ModelSerializer):
  class Meta():
    model = Merge
    fields = ('fileID', 'convertID')